<?php
require_once __DIR__ . '/functions/security.php';
get_header();

/*Template Name: Sobre*/
?>

<!-- O Escritório Section -->
<section id="sobre" class="conteudo-interna">
    <header class="header_interna">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">O escritório</h2>
                <h3 class="section-subheading">Gustavo Dias - Arquitetura e Design.</h3>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/escritorio.jpg" alt="">
            </div>
            <div class="col-md-7">
                <p>Gustavo Dias nasceu em são Paulo, no dia 09 de agosto de 1981, com 1 ano de idade mudou-
                    se para Ribeirão Preto onde vive atualmente. </p>
                <p>Concluiu a faculdade de arquitetura no Centro
                    universitário Moura Lacerda no ano de 2005 e em 2014 concluiu o curso de Iluminação e
                    Design de interiores pelo IPOG.</p>

                <p>Em 2006 começou a atuar na área desenvolvendo projetos e
                    administrando obras, e em 2010 consolidou o escritório de arquitetura Gustavo Dias
                    arquitetura e design.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-push-7">
                <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/escritorio2.jpg" alt="">
            </div>
            <div class="col-md-7 col-md-pull-5">
                <p>O escritório atua no mercado há 6 anos com projetos residenciais, comerciais e industriais,
                    com um estilo contemporâneo e um programa de projeto bem definido, visando a iluminação
                    e ventilação natural como pontos fortes dos seus projetos.</p>
                <p>Sempre dispostos a novos desafios
                    com muita criatividade e dinamismo executa um trabalho diferenciado, atualmente conta com
                    o apoio da arquiteta Christie Martins e da estagiária Rayane Valin, que compõe o corpo do
                    escritório.</p>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
