<?php
require_once __DIR__ . '/functions/security.php';
get_header();

/*Template Name: Projetos*/
?>

<!-- Projetos Section -->
<section id="projetos" class="conteudo-interna">
    <header class="header_interna">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Projetos</h2>
                <h3 class="section-subheading text-muted">Conheça os tipos de projetos atendidos pelo escritório.</h3>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="capsule-projeto">
            <div class="row">
                <div class="col-md-5 imagem">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/projeto-arquitetonico.jpg" alt="">
                </div>
                <div class="col-md-7 texto">
                    <h4>Projeto Arquitetônico</h4>
                    <p>Projeto deriva de projetar, do latim projectu, que significa lançar para diante. Nesse sentido, projetamos ideias para realizarmos sonhos em casas, condomínios, apartamentos e prédios.</p>
                    <p>Nesses projetos, visamos os aspectos arquitetônicos de seu desenvolvimento, de sua construção segundo as devidas especificações. Envolve coordenar as necessidades do cliente, ser designer e se relacionar com pessoal técnico como engenheiros estruturais, engenheiros mecânicos, engenheiros civis e paisagistas.</p>
                </div>
                <div class="col-md-12 capsule-lista">
                    <ul class="lista">
                        <li class="item label label-success">Abordagem que analisa informações e resolve problemas.</li>
                        <li class="item label label-success">Novos projetos com iniciativa e unidade.</li>
                        <li class="item label label-success">Comunicação transparente.</li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="capsule-projeto">
            <div class="row">
                <div class="col-md-5 imagem">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/projeto-industrial-varejo.jpg" alt="">
                </div>
                <div class="col-md-7 texto">
                    <h4>Projeto Industrial e Varejo</h4>
                    <p>A ideia da unidade produtiva vem mudando ao longo de tempo, desde a Revolução Industrial. Transformação significativa para a atual arquitetura, pois resultou em uma ampla variedade de práticas de planejamento e construção, muitos dos quais ainda são aplicados em projetos atuais. A intersecção entre arquitetura e indústria estimulou obras como edifício Bauakademie em Berlim (1831-1836), aglomerações como Highland Park de Henry Ford (1909), perto de Detroit projetado por Albert e Julius Kahn e Earnest Wilby Walter Gropius, a Fábrica Fagus de Adolf Meyer (1911) e subsequente Bauhaus de Gropius (1926).</p>
                </div>
                <div class="col-md-12">
                    <p>Para aplicação desse conceito hoje, projeta-se um espaço para abrigar uma indústria integrada, ao mesmo tempo, com o fornecimento de matéria-prima, com a destinação adequada de resíduos e com o envio apropriado do produto ao consumidor final. Materiais são pensados conforme suas caraterísticas funcionais como isolante, refletor, segurança, custos, durabilidade entre outras. Sempre com o máximo de iluminação natural e conforto. Afinal, desempenho está relacionado com bem estar no ambiente de trabalho.</p>
                </div>
                <div class="col-md-12 capsule-lista">
                    <ul class="lista">
                        <li class="item label label-success">Abordagem integrada e com visão sistêmica</li>
                        <li class="item label label-success">Design arrojado e gastos controlados</li>
                        <li class="item label label-success">Respeito às certificações</li>
                    </ul>
                </div>
            </div>
        </div>
         <hr>
        <div class="capsule-projeto">
            <div class="row">
                <div class="col-md-5 imagem">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/projeto-comercial.jpg" alt="">
                </div>
                <div class="col-md-7 texto">
                    <h4>Projeto Comercial</h4>
                    <p>Faltando texto</p>
                </div>
                <div class="col-md-12 capsule-lista">
                    <ul class="lista">
                        <li class="item label label-success">faltando</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
