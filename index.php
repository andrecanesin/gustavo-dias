<?php
require_once __DIR__ . '/functions/security.php';
get_header();
?>

<div class="home">
    <!-- Header -->
    <header id="home" class="principal_header">
        <ul class="bxslider-banner">

            <?php if (have_posts()) : ?>

                <!--faco o loop so da categoria "banner-home" e trago 3 posts-->
                <?php $recent = new WP_Query("cat=banner-home&showposts=3"); while($recent->have_posts()) : $recent->the_post();?>

                    <li class="banner" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>);"></li>

                <?php endwhile; ?>

                <?php else : ?>

                    Não há posts.

            <?php endif; ?>

        </ul>
    </header>

    <!-- Services Section -->
    <section class="destaques-home">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Projetos</h2>
                    <h3 class="section-subheading text-muted">Conheça os tipos de projetos atendidos pelo escritório.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="bloco-imagem">
                        <img class="imagem img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/projeto-arquitetonico.jpg" alt="">
                    </div>
                    <div class="bloco-conteudo clearfix">
                        <h4 class="service-heading">Projeto Arquitetônico</h4>
                        <p class="text-muted">Projeto deriva de projetar, do latim projectu, que significa lançar para diante. Nesse sentido, projetamos ideias para realizarmos sonhos em casas, condomínios, apartamentos e prédios.</p>
                        <a class="btn pull-right" href="#">leia mais</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bloco-imagem">
                        <img class="imagem img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/projeto-industrial-varejo.jpg" alt="">
                    </div>
                    <div class="bloco-conteudo clearfix">
                        <h4 class="service-heading">Projeto Industrial e varejo</h4>
                        <p class="text-muted">A ideia da unidade produtiva vem mudando ao longo de tempo, desde a Revolução Industrial. Transformação significativa para a atual arquitetura, pois resultou em uma ampla variedade de práticas de planejamento e construção, muitos dos quais ainda são aplicados em projetos atuais.</p>
                        <a class="btn pull-right" href="#">leia mais</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bloco-imagem">
                        <img class="imagem img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/projeto-comercial.jpg" alt="">
                    </div>
                    <div class="bloco-conteudo clearfix">
                        <h4 class="service-heading">Projeto Comercial</h4>
                        <p class="text-muted">Faltando texto.</p>
                        <a class="btn pull-right" href="#">leia mais</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Administracao Section -->
    <section id="administracao" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Gestão de Obras</h2>
                    <h3 class="section-subheading">Gerenciamos a execução de sua obra.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 galeria-adm">
                    <ul class="bxslider-adm">
                        <li>
                            <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm1.jpg" alt="Foto Administração de Obras">
                        </li>
                        <li>
                            <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm2.jpg" alt="Foto Administração de Obras">
                        </li>
                    </ul>
                </div>
                <div class="col-md-8 o-arquiteto">
                    <p><b>Gestão, do latim <i>Gestio</i>, denota ato de administrar. Logo, Administração de Obras significa gerenciar a execução das seguintes atividades:</b></p>
                    <ul class="lista">
                        <li class="item">Contratar os pedreiros, pintores, eletricistas, encanadores entre outros</li>
                        <li class="item">Comprar materiais com no mínimo três cotações com ótimos preços</li>
                        <li class="item">Escolha de todos os materiais, especialmente os acabamentos</li>
                        <li class="item">Apresentar relatórios mensais do planejado vs. Executado</li>
                        <li class="item">Acompanhar diariamente a execução da obra</li>
                        <li class="item">Abordagem profissional, eficiente e eficaz</li>
                        <li class="item">Gastos dentro do orçamento</li>
                        <li class="item">Transparência</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>

<?php get_footer() ?>