<?php
require_once __DIR__ . '/functions/security.php';
get_header();
?>

<!-- Navigation -->
<nav class="navbar navbar-home navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"></li>
                <li>
                    <a class="page-scroll" href="#sobre">
                        Sobre
                        <img src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover.png">
                        <img class="do-shrink" src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover-white.png">
                    </a>

                </li>
                <li>
                    <a class="page-scroll" href="#administracao">
                        Administração de Obras
                        <img src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover.png">
                        <img class="do-shrink" src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover-white.png">
                    </a>

                </li>
                <li>
                    <a class="page-scroll" href="/gustavo-dias/projetos">
                        Projetos
                        <img src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover.png">
                        <img class="do-shrink" src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover-white.png">
                    </a>
                </li>
                <li>
                    <a class="page-scroll" href="/gustavo-dias/obras">
                        Obras
                        <img src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover.png">
                        <img class="do-shrink" src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover-white.png">
                    </a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">
                        Contato
                        <img src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover.png">
                        <img class="do-shrink" src="<?php bloginfo('template_url'); ?>/static/img/main-menu-hover-white.png">
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header id="home" class="principal_header">
    <ul class="bxslider-banner">

        <?php if (have_posts()) : ?>

            <!--faco o loop so da categoria "banner-home" e trago 3 posts-->
            <?php $recent = new WP_Query("cat=banner-home&showposts=3"); while($recent->have_posts()) : $recent->the_post();?>

                <li class="banner" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>);"></li>

            <?php endwhile; ?>

            <?php else : ?>

                Não há posts.

        <?php endif; ?>

    </ul>
</header>

<section id="fake"></section>

<!-- About Section -->
<section id="sobre">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Sobre</h2>
                <h3 class="section-subheading">Gustavo Dias - Arquitetura e Design.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 img-sobre" style="background-image: url('<?php bloginfo('template_url'); ?>/static/img/escritorio.jpg');"></div>
            <div class="col-md-6 o-arquiteto">
                <p>Gustavo Dias nasceu em são Paulo, no dia 09 de agosto de 1981, com 1 ano de idade mudou-
                    se para Ribeirão Preto onde vive atualmente. </p>
                <p>Concluiu a faculdade de arquitetura no Centro
                    universitário Moura Lacerda no ano de 2005 e em 2014 concluiu o curso de Iluminação e
                    Design de interiores pelo IPOG.</p>

                <p>Em 2006 começou a atuar na área desenvolvendo projetos e
                    administrando obras, e em 2010 consolidou o escritório de arquitetura Gustavo Dias
                    arquitetura e design.</p>
            </div>
            <div class="col-md-12 sobre-destaque">
                <p>O escritório atua no mercado há 6 anos com projetos residenciais, comerciais e industriais,
                    com um estilo contemporâneo e um programa de projeto bem definido, visando a iluminação
                    e ventilação natural como pontos fortes dos seus projetos.</p>
                <p>Sempre dispostos a novos desafios
                    com muita criatividade e dinamismo executa um trabalho diferenciado, atualmente conta com
                    o apoio da arquiteta Christie Martins e da estagiária Rayane Valin, que compõe o corpo do
                    escritório.</p>
            </div>
        </div>
    </div>
</section>

<!-- Administracao Section -->
<section id="administracao" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Administração de Obras</h2>
                <h3 class="section-subheading">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 galeria-adm">
                <ul class="bxslider-adm">
                    <li>
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm1.jpg" alt="Foto Administração de Obras">
                    </li>
                    <li>
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm2.jpg" alt="Foto Administração de Obras">
                    </li>
                    <li>
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm3.jpg" alt="Foto Administração de Obras">
                    </li>
                    <li>
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm4.jpg" alt="Foto Administração de Obras">
                    </li>
                    <li>
                        <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/static/img/adm5.jpg" alt="Foto Administração de Obras">
                    </li>
                </ul>
            </div>
            <div class="col-md-8 o-arquiteto">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Contato</h2>
                <h3 class="section-subheading text-muted">Entre em contato conosco pelo formulário abaixo ou se preferir ligue para 16 3916-2059</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nome *" id="name" required data-validation-required-message="Preencha com seu nome.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="E-mail *" id="email" required data-validation-required-message="Preencha com seu e-mail.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Telefone *" id="phone" required data-validation-required-message="Preencha com seu telefone.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Preencha com sua mensagem *" id="message" required data-validation-required-message="Preencha com sua mensagem."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>