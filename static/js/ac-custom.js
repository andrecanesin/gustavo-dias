$(document).ready(function(){

    var mobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

    /* ---------------------------------------------- /*
     * Navbar Shrink
    /* ---------------------------------------------- */
    // $(window).scroll(function() {
    //   if ($(document).scrollTop() > 100) {
    //     $('.navbar-home').addClass('shrink');
    //   } else {
    //     $('.navbar-home').removeClass('shrink');
    //   }
    // });

    /* ---------------------------------------------- /*
     * Ir para o topo qdo carregar
    /* ---------------------------------------------- */
        // $('html, body').animate({
     //     scrollTop: 0
        // });

    /* ---------------------------------------------- /*
     * jQuery for page scrolling feature - requires jQuery Easing plugin
    /* ---------------------------------------------- */
    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });

    /* ---------------------------------------------- /*
     * WOW Animation When You Scroll
    /* ---------------------------------------------- */
    wow = new WOW({
        offset: 100,
        delay: '5s',
        mobile: false
    });
    wow.init();

    /* ---------------------------------------------- /*
     * Highlight the top nav as scrolling occurs
    /* ---------------------------------------------- */
    $('body').scrollspy({
        target: '.navbar'
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // ALTURA DO BANNER
    // $('li.banner').css('height', $(window).height());

    // ALTURA DO SOBRE
    $('.img-sobre').height($('.o-arquiteto').height());

    //CHAMANDO BXSLIDER BANNER
    $('.bxslider-banner').bxSlider({
        auto: true,
        infiniteLoop: true,
        pager: false,
        pause: 3000,
        easing: 'easeInOut',
        speed: 1000,
        nextText: '',
        prevText: ''
    });

    //CHAMANDO BXSLIDER ADM
    $('.bxslider-adm').bxSlider({
        auto: false,
        infiniteLoop: true,
        pager: false,
        easing: 'easeInOut',
        speed: 1000,
        nextText: '',
        prevText: ''
    });

    //CHAMANDO SWIPEBOX
    $('.swipebox').swipebox();

    // ISOTOPE
    if ( $('.iso-box-wrapper').length > 0 ) {

        var $container  = $('.iso-box-wrapper'),
            $imgs       = $('.iso-box img');

        $container.imagesLoaded(function () {

            $container.isotope({
                layoutMode: 'fitRows',
                itemSelector: '.iso-box'
            });

            $imgs.load(function(){
                $container.isotope('reLayout');
            })

        });

        // filter items on button click
        $('.filter-wrapper li a').click(function(){

            $('.filter-wrapper li a').removeClass("current");
            $(this).addClass("current");

            var $this = $(this), filterValue = $this.attr('data-filter');

            $container.isotope({
                filter: filterValue,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });

            // don't proceed if already selected
            if ( $this.hasClass('selected') ) {
                return false;
            }

            var filter_wrapper = $this.closest('.filter-wrapper');
            filter_wrapper.find('.selected').removeClass('selected');
            $this.addClass('selected');

          return false;
        });
    }



});