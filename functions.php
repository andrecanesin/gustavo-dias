<?php
require_once __DIR__ . '/functions/security.php';
require_once __DIR__ . '/functions/services/service-contact-form.php';
require_once __DIR__ . '/functions/controllers/controller-single.php';
require_once __DIR__ . '/functions/controllers/controller-contact.php';

if ( function_exists( 'register_nav_menu' ) ) {
    register_nav_menu( 'menu_1', 'primeiro-menu' );
}

add_filter('show_admin_bar', '__return_false');

add_post_type_support( 'page', 'excerpt' );

remove_action('wp_head', 'wp_generator');

add_action( 'after_setup_theme', 'setup_features' );
function setup_features() {
    add_theme_support( 'post-thumbnails' );
}

// add category nicenames in body and post class
function category_id_class( $classes ) {
    global $post;
    foreach ( ( get_the_category( $post->ID ) ) as $category ) {
        $classes[] = $category->category_nicename;
    }
    return $classes;
}
add_filter( 'post_class', 'category_id_class' );
add_filter( 'body_class', 'category_id_class' );

