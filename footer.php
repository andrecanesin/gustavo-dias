<?php require_once __DIR__ . '/functions/security.php' ?>

<?php wp_footer() ?>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Gustavo Dias - Arquitetura e Design</p>
                    <p>CAU: A47166-6</p>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="endereco">
                        <address>
                            Av. Áurea A. Bragheto Machado, 161<br>Ribeirão Preto - SP
                        </address>
                        <p>16 3916-2-59/16 99132-7625</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php bloginfo('template_url'); ?>/static/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php bloginfo('template_url'); ?>/static/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/classie.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/jquery.bxslider.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/isotope.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/imagesloaded.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/jquery.swipebox.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/wow.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php bloginfo('template_url'); ?>/static/js/jqBootstrapValidation.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/static/js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php bloginfo('template_url'); ?>/static/js/ac-custom.js"></script>

</body>

</html>
