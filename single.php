<?php
require_once __DIR__ . '/functions/security.php';
get_header();
?>


<?php the_post(); ?>

<!-- Portfolio Grid Section -->
<article class="single">
    <div class="single-destaque">
        <?php the_post_thumbnail( 'portfolio', array( 'class' => 'custom-class img-responsive' ) ); ?>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 single-content bg-light-gray">
                        <a class="zoom swipebox" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"></a>
                        <h2 class="section-heading"><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <section class="bg-light-gray">
        <div class="container">
            <h3 class="section-subheading text-center">Outras fotos da obra</h3>
            <ul class="row">
            <?php
                $args = array('post_parent'=>$post->ID,'post_type'=>'attachment','post_mime_type'=>'image/jpeg');
                $my_attachments = get_posts($args);
            ?>
            <?php if ($my_attachments): ?>
                <?php foreach($my_attachments as $image): ?>

                    <li class="item-outras-fotos col-md-6">
                        <a class="swipebox zoom" title="<?php echo $image->post_title ;?>" href="<?php echo $image->guid ;?>"><i class="fa fa-search"></i></a>
                        <?php echo wp_get_attachment_image($image->ID, 'img-responsive'); ?>
                    </li>
                <?php endforeach; ?>
                <?php else:?>
                    <p>Nenhuma imagem foi inserida.</p>
                <?php endif;?>
            </ul>
        </div>
    </section>
</article>

<?php get_footer() ?>
