
 <ul>
    <?php foreach ( $images as $image ) : ?>
        <li>
            <a href="<?php echo $image->imageURL ?>" title="<?php echo $image->description ?>" <?php echo $image->thumbcode ?> >
        <?php if ( !$image->hidden ) { ?>
            <img title="<?php echo $image->alttext ?>" alt="<?php echo $image->alttext ?>" src="<?php echo $image->thumbnailURL ?>" <?php echo $image->size ?> />
        <?php } ?>
        </a>
        </li>
    <?php endforeach; ?>
</ul>