<?php
require_once __DIR__ . '/functions/security.php';
get_header();

/*Template Name: Contato*/
?>

<!-- Contact Section -->
<section id="contact" class="conteudo-interna">
    <header class="header_interna">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Contato</h2>
                <h3 class="section-subheading text-muted">Entre em contato conosco pelo formulário abaixo ou se preferir ligue para 16 3916-2059</h3>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nome *" id="name" required data-validation-required-message="Preencha com seu nome.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="E-mail *" id="email" required data-validation-required-message="Preencha com seu e-mail.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Telefone *" id="phone" required data-validation-required-message="Preencha com seu telefone.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Preencha com sua mensagem *" id="message" required data-validation-required-message="Preencha com sua mensagem."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
