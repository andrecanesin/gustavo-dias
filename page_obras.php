<?php
require_once __DIR__ . '/functions/security.php';
get_header();

/*Template Name: Obras*/
?>

<!-- Portfolio Grid Section -->
<section id="obras" class="portfolio bg-light-gray" class="conteudo-interna">
    <header class="header_interna">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Obras</h2>
                <h3 class="section-subheading text-muted">Confira nossas obras</h3>
            </div>
        </div>

        <ul class="filter-wrapper clearfix">
            <li><a class="current" data-filter="*">Todos</a></li>
            <li><a class="opc-main-bg" data-filter=".comercial">Comercial</a></li>
            <li><a class="opc-main-bg" data-filter=".industrial-varejo">Industrial e Varejo</a></li>
            <li><a class="opc-main-bg" data-filter=".residencial">Residencial</a></li>
        </ul>
    </header><!-- /header -->
    <div class="container">
        <ul class="row iso-box-wrapper">

                <!-- The Query -->
                <?php query_posts();?>

                <?php
                    $classes = array(
                        'col-md-4',
                        'col-sm-6',
                        'portfolio-item',
                        'iso-box',
                    );
                ?>

                <!-- The Loop -->
                <?php while ( have_posts() ) : the_post(); ?>

                    <li <?php post_class( $classes ); ?>>
                        <a href="<?php the_permalink() ?>" class="portfolio-link">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content">
                                    <i class="fa fa-plus"></i>
                                </div>
                            </div>
                            <?php the_post_thumbnail( 'portfolio', array( 'class' => 'custom-class img-responsive' ) ); ?>
                        </a>
                        <div class="portfolio-caption">
                            <h4><?php the_title(); ?></h4>
                            <p class="text-muted"><?php the_category(', ') ?></p>
                        </div>
                    </li>

                <?php endwhile; ?>

                <!-- Reset Query -->
                <?php wp_reset_query(); ?>

        </ul>

    </div>
</section>

<?php get_footer() ?>
